import { useEffect } from "react";

const SaveGPS = ({ gpsPosition, setGPSPositions }) => {
  useEffect(() => {
    const interval = setInterval(() => {
      setGPSPositions([...gpsPosition, "4.765765"]);
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  });
  return <></>;
};

export default SaveGPS;
