const DisplayGPS = ({ gpsPosition }) => (
  <ul>
    {gpsPosition.map((el) => (
      <li>{el}</li>
    ))}
  </ul>
);

export default DisplayGPS;
