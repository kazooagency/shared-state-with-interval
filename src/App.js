import { useState } from "react";

import "./App.css";
import SaveGPS from "./SaveGPS";
import DisplayGPS from "./DisplayGPS";

function App() {
  const [gpsPosition, setGPSPositions] = useState(["2.987", "3.98987"]);
  return (
    <div className="App">
      <p>Je suis app</p>
      <SaveGPS gpsPosition={gpsPosition} setGPSPositions={setGPSPositions} />
      <DisplayGPS gpsPosition={gpsPosition} />
    </div>
  );
}

export default App;
